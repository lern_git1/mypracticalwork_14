﻿#include <iostream>
#include <string>

int main()
{
    std::cout << "Введите строку: ";
    std::string AnyString;
    std::getline(std::cin, AnyString);
    std::cout << AnyString;
    std::cout << AnyString.length() << "\n";
    std::cout << "Первый символ строки: " << AnyString[0] << "\n";
    std::cout << "Последний символ строки: " << AnyString.back() << '\n';
}